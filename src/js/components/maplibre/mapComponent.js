import Vue from 'vue';
import {DeferredReady} from '../../deferredReady';

Vue.use(DeferredReady);

export default Vue.extend({
  mixins: [DeferredReady],
  created () {
    this.$lmap = null;
    this.$on('map-ready',
            function (map) {
              this.$lmap = map;
            }
        );
  },
  deferredReady () {
    this.$emit('register-component', this);
  }
});
