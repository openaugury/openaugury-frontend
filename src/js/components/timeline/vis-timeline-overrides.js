function repaintMajorLine(heightFunction, left, width, orientation, className) {
  const lineWidth = width / 3;
  const minorLineWidth = width / 5;
  if (lineWidth < 4) {
    return null;
  }

  // reuse redundant line
  let line = this.dom.redundant.lines.shift();
  if (!line) {
          // create vertical line
          line = document.createElement('div');
          this.dom.background.appendChild(line);
        }
  this.dom.lines.push(line);

  const props = this.props;
  
  line.style.width = `${lineWidth}px`;
  line.style.height = `${props.majorLineHeight}px`;

  let y = (orientation == 'top') ? 0 : this.body.domProps.top.height;
  let x = left - lineWidth / 2;

  this._setXY(line, x, y);
  line.className = `vis-grid ${this.options.rtl ?  'vis-vertical-rtl' : 'vis-vertical'} vis-major ${className}`;

  repaintMinorLine.bind(this)(heightFunction, left, width, orientation, className);

  return line;
}

function repaintMinorLine(heightFunction, left, width, orientation, className) {
  const lineWidth = width / 5;
  if (lineWidth < 4) {
    return null;
  }

  for (let i = 0 ; i < 5 ; i++) {
    // reuse redundant line
    let line = this.dom.redundant.lines.shift();
    if (!line) {
      // create vertical line
      line = document.createElement('div');
      this.dom.background.appendChild(line);
    }
    this.dom.lines.push(line);

    const props = this.props;
    const tickLeft = left + i * lineWidth;
    const [risk, inSimulation] = heightFunction(this.body.util.toTime(tickLeft));
    line.style.width = `${lineWidth - 3}px`;
    line.style.height = `${20 * (1 + 2 * risk)}px`;
    line.style.bottom = `0px`;

    let y = (orientation == 'top') ? props.majorLabelHeight : this.body.domProps.top.height;
    let x = tickLeft - lineWidth / 2;

    this._setXY(line, x, y);
    line.className = `vis-grid ${this.options.rtl ?  'vis-vertical-rtl' : 'vis-vertical'} vis-minor ${className}`;
    if (inSimulation) {
      line.classList.add('in-simulation');
    }

  }
  return null;
}

export default { repaintMajorLine, repaintMinorLine };
