module.exports = {
  mapboxAccessToken: process.env.VUE_APP_MAPBOX_ACCESS_TOKEN,
  providers: {
    'ar_SA': {
      'monochrome': process.env.VUE_APP_MAPBOX_STYLE_AR,
      'terrain': process.env.VUE_APP_MAPBOX_STYLE_AR
    },
    'default': {
      'monochrome': 'Stamen.Toner',
      'terrain': 'Stamen.Terrain'
    }
  }
};
