const webpack = require('webpack');

module.exports = {
  publicPath: process.env.VUE_APP_PUBLIC_PATH,
  runtimeCompiler: true,

  devServer: {
    disableHostCheck: true,
    watchOptions: {
      ignored: /node_modules/,
      poll: 1000,
    },
  },
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
       })
    ]
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
};

