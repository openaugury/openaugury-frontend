// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage
// automatically uses dev Server port from /config.index.js
// default: http://localhost:8080
// see nightwatch.conf.js

module.exports = {
  '@tags':['panel'],
  'Title': function (browser) {
    const devServer = browser.globals.devServerURL 
    const simToken = browser.globals.simToken 
    browser
      .url(devServer)
      .waitForElementVisible('#app', 20000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .setValue('#tokenText',simToken)
      .click("#submitToken")
      .pause(20000)
      .waitForElementVisible('#panel-page',5000)
      .assert.title('OpenAugury Map Viewer')
      .end();

  },
  'NewsFeed Popup': function (browser) {
    const devServer = browser.globals.devServerURL 
    const simToken = browser.globals.simToken 
    browser
      .url(devServer)
      .waitForElementVisible('#app', 10000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .setValue('#tokenText',simToken)
      .click("#submitToken")
      .pause(20000)
      .waitForElementVisible('#panel-page',5000)
      .assert.hidden('#newsfeed')
      .click('#clickNewsFeed')
      .waitForElementVisible('#newsfeed',100)
      .end()
  },
  'Task Popup': function (browser) {
    const devServer = browser.globals.devServerURL 
    const simToken = browser.globals.simToken 
    browser
      .url(devServer)
      .waitForElementVisible('#app', 10000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .setValue('#tokenText',simToken)
      .click("#submitToken")
      .pause(20000)
      .waitForElementVisible('#panel-page',5000)
      .click('#clicktaskwork')
      .end();

  },
  'Layer Menu': function (browser) {
    const devServer = browser.globals.devServerURL 
    const simToken = browser.globals.simToken 
    browser
      .url(devServer)
      .waitForElementVisible('#app', 10000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .setValue('#tokenText',simToken)
      .click("#submitToken")
      .pause(20000)
      .waitForElementVisible('#panel-page',5000)
      .click('#dialogMenu')
      .waitForElementVisible('#layerMenu',500)
      .end();
  }
}
