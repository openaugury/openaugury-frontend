// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  '@tags': ['modal'],
  'Token': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .waitForElementVisible('#app', 40000)
      .waitForElementVisible('#tokenModal', 40000)
      .assert.elementPresent('#submitToken')
      .end()
  },
  'Submit': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL
    const simToken = browser.globals.simToken

    browser
      .url(devServer)
      .waitForElementVisible('#app', 40000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .setValue('#tokenText',simToken)
      .click("#submitToken")
      .pause(40000)
      .waitForElementVisible('#panel-page',10000)
      .end()
  },
  'Submit without token': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .waitForElementVisible('#app', 40000)
      .assert.elementPresent('#tokenModal')
      .assert.elementPresent('#submitToken')
      .clearValue('#tokenText')
      .click("#submitToken")
      .pause(5000)
      .end()
  }
}
